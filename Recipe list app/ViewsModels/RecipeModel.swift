//
//  RecipeModel.swift
//  Recipe list app
//
//  Created by Lola Garavagno on 21/09/2021.
//

import Foundation

class RecipeModel:ObservableObject {
    
    @Published var recipes = [Recipe] ()
    
    init() {
        // Create an instance of data service and get the data

        self.recipes = DataService.getLocalData()
        
        // Set the recipes property
    }
    
}
