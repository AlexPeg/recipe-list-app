//
//  Recipe_list_appApp.swift
//  Recipe list app
//
//  Created by Lola Garavagno on 21/09/2021.
//

import SwiftUI

@main
struct Recipe_list_app: App {
    var body: some Scene {
        WindowGroup {
            RecipeTabView()
        }
    }
}
