//
//  RecipeDetailView.swift
//  Recipe list app
//
//  Created by Lola Garavagno on 23/09/2021.
//

import SwiftUI

struct RecipeDetailView: View {
    
    var recipe: Recipe
    var body: some View {
        ScrollView {
            
            VStack(alignment: .leading) {
            
            // Recipe Image
            
            Image(recipe.image)
                .resizable()
                .scaledToFill()
            
            // Ingredients
            VStack(alignment:.leading) {
            
            Text("Ingredients")
                .font(.headline)
                .padding([.bottom, .top],5)
                
                ForEach (recipe.ingredients) { item in
                    Text(item.name)
                        
                }
            }
            .padding(.horizontal)
            
            // Directions
            VStack(alignment:.leading) {
            
            Text("Directions")
                .font(.headline)
                .padding([.bottom, .top],5)
                
                ForEach (recipe.directions, id:\.self) { item in
                    Text(item)
                        .padding(.bottom,5)
                        
                }
            }
            .padding(.horizontal)
            
        }
            .navigationBarTitle(recipe.name)
    }
}

struct RecipeDetailView_Previews: PreviewProvider {
    static var previews: some View {
        
        let model = RecipeModel()
        RecipeDetailView(recipe: model.recipes[0])
    }
}
}
